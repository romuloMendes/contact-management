<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{
    CustomAuthController,
    HomeController
};
use App\Http\Controllers\People\PeopleController;

/*
|--------------------------------------------------------------------------
| Web Routes publics
|--------------------------------------------------------------------------
*/
Route::get('/', function () {
    return redirect('/login');
});

Route::get('signout', [CustomAuthController::class, 'signOut'])->name('signout');

/*
|--------------------------------------------------------------------------
| Web Routes private
|--------------------------------------------------------------------------
*/
Auth::routes();
Route::get('/dashboard/index', [CustomAuthController::class, 'dashboard'])->name('dashboard');
Route::get('/peoples', [PeopleController::class, 'index'])->name('peoples.index');
Route::get('/peoples/create', [PeopleController::class, 'create'])->name('peoples.create');
Route::post('/peoples/store', [PeopleController::class, 'store'])->name('peoples.store');

Route::get('/home', [HomeController::class, 'index'])->name('home');
