<?php

namespace App\Http\Controllers\People;

use App\Http\Controllers\Controller;
use App\Models\Peoples;
use Illuminate\Http\Request;

class PeopleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Peoples $people)
    {
        $this->middleware('auth');
        $this->model = $people;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $peoples = $this->model->paginate();
        return view('peoples.index', compact('peoples'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('peoples.create');
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        dd("store");
        // return view('peoples.create');
    }
}
