@extends('layouts.app')

@section('content')
<div class="d-flex justify-content-center align-items-center pt-5">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title text-center m-0 p-1">Cadastro de produtos</h4>
        </div>
        <form class="bg-white p-5 border shadow" action="{{ route('peoples.store') }}" method="POST">
            @csrf
            <div class="form-group">
                <label>Nome do produto</label>
                <input onkeyup="this.value = this.value.toUpperCase()" class="form-control" name="nome_produto" placeholder="Nome">
            </div>

            <div class="d-flex justify-content-between">
                <div class="form-group col-sm-5 p-0">
                    <label>Valor</label>
                    <input type="number" name="valor" class="form-control" placeholder="Valor">
                </div>
                <div class="form-group col-sm-5 p-0">
                    <label>Quantidade</label>
                    <input type="number" name="quantidade" class="form-control" placeholder="Quantidade">
                </div>
            </div>
            <button type="submit" class="btn btn-success">Cadastrar</button>
        </form>
    </div>
</div>
@endsection
