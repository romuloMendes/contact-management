@extends('layouts.app')

@section('content')

<table class="min-w-full leading-normal shadow-md rounded-lg overflow-hidden">
    <thead>
        <tr>
            <th
                class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-700 uppercase tracking-wider">
                Nome
            </th>
            <th
                class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-700 uppercase tracking-wider">
                E-mail
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($peoples as $people)
            <tr>
                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">{{( $people->name) }}</td>
                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">xx</td>
            </tr>
        @endforeach
    </tbody>
</table>
@endsection
