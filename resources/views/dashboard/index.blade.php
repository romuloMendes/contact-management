@extends('layouts.app')
@section('content')
    <div class="fixed-bottom float-right d-flex justify-content-end m-3">
        <a href="{{route('peoples.create')}}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Novo contato</a>
    </div>
@endsection
